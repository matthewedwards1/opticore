# OptiCore

This repository contains driver and application software for optical neural network research and development. Optical neural networks (ONNs) systems composed of are photonic integrated circuits (PICs) and electronic designed designed to perform deep learning tasks. Photonics researchers are developing PICs to accelerate and make efficient neural network training and inference. This repository contains software developed to support these efforts.

Some introductory work on deep learning iwth optical neural networks:
- [Reprogrammable Electro-Optic Nonlinear Activation Functions for Optical Neural Networks](https://arxiv.org/abs/1903.04579)
- [Experimental Realization of Arbitrary Activation Functions for Optical Neural Networks](https://www.osapublishing.org/oe/viewmedia.cfm?uri=oe-28-8-12138&seq=0)

## Contents

- `flask`: A flask app for controlling optical neural network systems.
- `math`: A python module containing mathematics for ONN training and inference.

## Contributing

All contrabutions are welcome and encouraged. Please email contact@matthewedwards.io with any questions.

## License

This repository is licensed under the MIT license. The full license text may be found in the `LICENSE` file.

## Documentation

[Visit this page](https://matthewedwards1.gitlab.io/opticore/)
