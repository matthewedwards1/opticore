from functools import reduce

import numpy as np
from scipy.linalg import block_diag as block
from scipy.stats import unitary_group as unitary

def mprint(matrix):
    """ Print a matrix in a nice format
    """
    print(np.array(matrix).round(decimals=3))

def get_decomp_matrix(a, b, mu=1):
    """ Calculate a unitary matrix factor

    Arguments
        a: The first coef
        b: The second coef
        mu: Any number
    Returns:
        A 2x2 numpy.array
    """
    u = (np.abs(a)**2 + np.abs(b)**2)**.5
    decomp_matrix = np.array([[mu*np.conj(a)/u, mu*np.conj(b)/u], 
                              [           -b/u,             a/u]])
    return decomp_matrix, u

class SquareDecomposition():
    """ A class for generating square unitary decompositions as detailed in 
        https://arxiv.org/pdf/1603.08788.pdf
    """
    @staticmethod
    def decompose(mat):
        """ Decompose a unitary matrix into (N)(N-1)/2 unitary matrices. See the arxiv paper
        supplement for pseudo-code of the matrix decomposition algorithm.

        Arguments:
            mat (numpy.array): An complex array with shape NxN.

        Returns:
            A three-tuple. The first and third elements of the tuple are arrays of unitary
            transforms in a 2-dimensional subspace of the overall unitary transform. The second
            element is a diagonal matrix corresponding to single mode phase shifts. See the formula
            at the bottom of page 7 of the arxiv paper.
        """
        dim = mat.shape[0]
        left_decomp = np.ndarray((0, dim, dim))
        right_decomp = np.ndarray((0, dim, dim))
        D = np.array(mat) # 'D' matrix as notated in the arxiv paper linked above.
        for i in range(1, dim):
            for j in range(i):
                # Get 2D unitary transform implemented by a single MZI
                if i % 2:
                    val_1 = D[dim - 1 - j][i - j - 1]
                    val_2 = D[dim - 1 - j][i - j]
                    decomp_mat, _ = get_decomp_matrix(1/np.conj(val_1), 1/val_2)
                else:
                    val_1 = D[dim - 0 + j - i][j]
                    val_2 = D[dim - 1 + j - i][j]
                    decomp_mat, _ = get_decomp_matrix(1/val_1, 1/val_2)

                # Place the 2D unitary transform in the correct 2D subspace of the overall transform
                block_left = np.eye(i-j-1) if i % 2 else np.eye(dim+j-i-1)
                block_right = np.eye(dim-(i-j+1)) if i % 2 else np.eye(dim-2-(dim+j-i-1))
                decomp_mat = block(block_left, decomp_mat, block_right)
                
                # Apply the decomposition, save the factor matrix
                if i % 2:
                    D = np.matmul(np.array(D), np.array(decomp_mat))
                    right_decomp = np.append(right_decomp, np.linalg.inv(decomp_mat.reshape(1, dim, dim)), axis=0)
                else:
                    D = np.matmul(np.array(decomp_mat), np.array(D))
                    left_decomp = np.append(left_decomp, decomp_mat.reshape(1, dim, dim), axis=0)
        return left_decomp, D, right_decomp

    @staticmethod
    def reconstruct(left_decomp, D, right_decomp):
        left_decomp[0] = np.linalg.inv(left_decomp[0])
        left = reduce(lambda x, y: np.matmul(x, np.linalg.inv(y)), left_decomp)
        right = reduce(lambda x, y: np.matmul(x, y), right_decomp[-1::-1])
        return np.matmul(left, np.matmul(D, right))

class TriangularDecomposition():
    """ A class for generating triangular unitary decompositions as detailed in 
        https://arxiv.org/pdf/1210.7366.pdf

        This class contains two public functoins. decompose() and reconstruct(). Decompose takes a
        NxN unitary matrix and yields the triangular unitary decomposition. The triangular unitary
        decomposition is composed of (N)(N-1)/2 unitary matrices which transform a 2-dimensional
        subspace of the original unitary matrix. reconstruct() reproduces the original matrix from
        the triangular unitary decomposition. Assuming all matrices passed to decompose() are
        unitary, decompose() and reconstruct() are inverses.
    """

    @staticmethod
    def decompose(mat):
        """ Decompose a unitary matrix into (N)(N-1)/2 unitary matrices. 

        Arguments:
            mat (numpy.array): An complex array with shape NxN.

        Returns:
            A numpy.array with shape (N)(N-1)/2 X N X N. Iterating along the first axis yields the
            indivdual unitary matrix factors.
        """
        dim = mat.shape[0]
        full_decomp = np.ndarray((0, dim, dim))
        matrix = np.array(mat)
        v_mats = list()
        for redux in range(dim - 2):
            decomp = TriangularDecomposition._reduce_dim(matrix)
            matrix = TriangularDecomposition._factor_1d(decomp, matrix)[1:, 1:]
            v_mats.append(matrix)
            for d in range(len(decomp)):
                decomp[d] = block(np.eye(redux), decomp[d])
            full_decomp = np.concatenate((full_decomp, decomp), axis=0)
        v = block(np.eye(dim - 3), v_mats[-2])
        final = TriangularDecomposition._final_unitary(full_decomp, v).reshape((1, dim, dim))
        full_decomp = np.append(full_decomp, final, axis=0)
        return full_decomp

    @staticmethod
    def reconstruct(decomp):
        """ Reconstruct a matrix from a unitary decomposition

        Arguments:
            decomp (numpy.array): A (N)(N-1)/2 X N x N matrix. Decomp should be acquired from a call
                to TriangularDecomposition.decompose().
        Returns:
            The matrix reconstructed from the decomposition 
        """
        decomp_mul = np.array(decomp)
        decomp_mul[0] = decomp_mul[0].T.conjugate()
        return reduce(lambda x, y: np.matmul(x, y.T.conjugate()), decomp_mul)

    @staticmethod
    def _reduce_dim(matrix, col=0, mu=None):
        """ Reduces the dimension of a unitary matrix by preforming a unitary matrix decomposition

        Arguments:
            matrix (numpy.array): A square complex-valued matrix of shape NxN.
            col (int): The column over which the dimension is reduced.
            mu (iterable): An iterable of complex values. Length must N.
        Returns:
            A list of numpy.array of shape NxN. 
        """
        # Parse inputs
        dim = matrix.shape[0]
        mu = [1]*dim if mu is None else mu 
        
        # Validate inputs
        for shape in matrix.shape:
            if shape != dim:
                raise ValueError("Matrix must be square")
        if dim <= 1:
            raise ValueError("Cannot reduce matrix dimension lower than 1")
        if col < 0 or col >= dim:
            raise ValueError("Column does not exist in matrix")
        if len(mu) != dim:
            raise ValueError("Invalid mu array passed. Length of mu array must equal matrix dimension")

        # Decompose
        decomp = list()
        u = None
        for i, mu, in zip(list(range(dim-1))[-1::-1], mu):
            a = matrix[i, col]
            b = matrix[i+1, col] if u is None else u
            decomp_mat, u = get_decomp_matrix(a, b, mu)
            decomp_mat = block(np.eye(i), decomp_mat, np.eye(dim - i - 2))
            decomp.append(decomp_mat)
        return decomp

    @staticmethod
    def _factor_1d(decomp, mat):
        """ Reduce a matrix by one dimension based on a 1 dimensional unitary matrix decomposition

        Arguments:
            mat (numpy.array): An NxN complex unitary matrix
            decomp (numpy.array): 

        """
        decomp_mul = np.array(decomp)
        mat_mul = np.array(mat)
        return np.matmul(reduce(lambda x, y: np.matmul(x, y), decomp_mul[-1::-1]), mat_mul)

    @staticmethod
    def _final_unitary(decomp, mat):
        decomp_mul = np.array(decomp)
        matrix = np.array(mat)
        return np.linalg.inv(np.matmul(np.matmul(decomp_mul[-1], decomp_mul[-2]), matrix))

def mat2phase(mat):
    """ Convert a unitary matrix factor into phi and theta phase values for a unitary matrix of
    the form:

        [[ e^{ip} * cos(t), -sin(t) ]
         [ e^{ip} * sin(t),  cos(t) ]]

    where p is phi and t is theta.

    Arguments:
        mat (numpy.array): An NxN matrix. The matrix is an identity matrix aprt from a 2x2 block
            of non-zero values on the main diagonal. This matrix is generated by
            TriangularDecomposition or SquareDecomposition.
    Returns:
        a 2-tuple of (theta, phi)
    """
    for dim in range(len(mat)):
        if mat[dim][dim] == 1:
            continue
        phi1 = np.arctan(np.imag(mat[dim][dim])/np.real(mat[dim][dim]))
        phi2 = np.arctan(np.imag(mat[dim][dim+1])/np.real(mat[dim][dim+1]))
        phi = (phi1 + phi2) / 2
        theta = np.real(np.arccos(mat[dim][dim]/np.exp((0+1j)*phi1)))
        
        return (theta, phi)
    raise ValueError("mat has no non-unitary terms on the major axis")

if __name__ == '__main__':
    import sys

    # Demonstrate decomposition and reconstruction
    dim = 4
    mat = unitary.rvs(dim) # Generate a random unitary matrix

    # Perform test
    if 'triangle' in sys.argv:
        decomp = TriangularDecomposition.decompose(mat)
        recon  = TriangularDecomposition.reconstruct(decomp)
        print(mat2phase(decomp[0]))
    if 'square' in sys.argv:
        left, D, right = SquareDecomposition.decompose(mat)
        mprint(left)
        mprint(right)
        print(mat2phase(left[0]))
        recon = SquareDecomposition.reconstruct(left, D, right)

    # Check test
    if sum(sum(recon.round(12) == mat.round(12))) == dim**2: # Round to remove floating point errors
        print("Success")
    else:
        print("Failure")
