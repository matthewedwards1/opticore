# -*- coding: utf-8 -*-
import multiprocessing
import os
import requests
import socket
import sys
import unittest
from unittest.mock import patch

from board_debug.startup import start
from board_debug.devices import RF_INTERFACE_SWITCHES


class APITests(unittest.TestCase):
    ip = 'localhost'
    port = 8000
    url = f'http://{ip}:{port}/'


    def setUp(self):
        # Surpress flask output
        os.makedirs('log', exist_ok=True)
        self.f = open(os.path.join('log', 'flask.log'), 'w')
        sys.stdout = self.f

        # Start flask server
        self.server = multiprocessing.Process(target=start)
        self.server.start()
        self.wait_for_server()

    def tearDown(self):
        # Close flask server
        self.server.kill()
        self.server.join()

        # Close flask output file redirect
        self.f.close()

    def wait_for_server(self):
        s = socket.socket()
        while True:
            try:
                s.connect((self.ip, self.port))
                s.close()
                return
            except Exception:
                pass

    def req_func(self, command, data):
        response = requests.get("".join([self.url, command]), params=data)
        self.assertEqual(response.status_code, 200)        

    def test_no_crash(self):
        """ Test all the API endpoints with a single value set of arguments
        """

        self.req_func('rf_interface_dsa', {'dsa': 1, 'value': 0})
        self.req_func('rf_interface_switches', {'switch': 1, 'value': 0})
        self.req_func('rx_processing_switches', {'switch': 0, 'value': 0})
        self.req_func('rx_processing_io_expander', {'switch': 1, 'value': 0})
        self.req_func('rx_processing_dac', {'dac': 1, 'value': 0})
        self.req_func('rx_processing_dsa', {'dsa': 0, 'value': 0})
        self.req_func('tx_processing_dsa', {'dsa': 1, 'value': 0})
        self.req_func('tx_processing_switches', {'switch': 1, 'value': 0})
        self.req_func('tx_lo', {'freq': 200e6})
        self.req_func('tx_laser_bias', {'bias': 1})
        self.req_func('6tapfir_dsa', {'board': 1, 'filter': 1, 'tap': 1, 'phase': 1, 'atten': 1})
        self.req_func('6tapfir_vva', {'board': 1, 'filter': 1, 'value': 1})
        self.req_func('6tapfir_switches', {'board': 1, 'filter': 1, 'tap': 1, 'value': 1})
        self.req_func('evk_defaults', {'target': 'all'})

    def test_rf_interface_dsa_valid(self):
        dsa = range(1, 6)
        value = range(0, 4095, 100)
        for d in dsa:
            for v in value:
                self.req_func('rf_interface_dsa', {'dsa': d, 'value': v})

    def test_rf_interface_switches_valid(self):
        
        switch = [RF_INTERFACE_SWITCHES.keys(), RF_INTERFACE_SWITCHES.values()]
        value = range(0, 1)
        for s in switch:
            for v in value:
                self.req_func('rf_interface_switches', {'switch': s, 'value': v})

if __name__ == '__main__':
    unittest.main()
