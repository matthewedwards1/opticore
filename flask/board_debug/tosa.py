# -*- coding: utf-8 -*-
from board_debug.dacx5080 import dacx5080_set_channel, dacx5080_config, dacx5080_gain
from board_debug.sn74hc595pwr import format_message as shift_reg_format
from board_debug.pic_dac import pic_dac_bytes
from board_debug.amp import amp_bytes
from board_debug.spi import send

LASER_BIAS_CS = 1
SHIFT_REG_CS  = 2
PIC_DAC_CS    = 3 
AMP_SHIFT_CS  = 5
AMP_DAC_CS    = 6

class TOSAState():
    tec = 0
    _dict = {}
    def __init__(self):
        self.__dict__ = self._dict

def dac_bytes(val):
    return bytearray.fromhex("{0:0{1}x}".format(val,6))

def tosa_laser_bias_send(val_bytes):
    send(val_bytes, cs_num=LASER_BIAS_CS, direct=True, spimode=1)

def tosa_shift_reg_send(val_bytes):
    send(val_bytes, cs_num=SHIFT_REG_CS, direct=True, spimode=0)

def tosa_pic_dac_send(val_bytes):
    send(val_bytes, cs_num=PIC_DAC_CS, direct=True, spimode=1)

def tosa_amp_bias_send(val_bytes):
    send(val_bytes, cs_num=AMP_DAC_CS, direct=True, spimode=1)

def tosa_amp_shift_reg_send(val_bytes):
    send(val_bytes, cs_num=AMP_SHIFT_CS, direct=True, spimode=0)

def tosa_laser_bias(chn, value):
    val = dacx5080_config()
    val_bytes = dac_bytes(val)
    tosa_laser_bias_send(val_bytes)

    val = dacx5080_gain(gain=False, ref=False) #enabling gain could risk damage to laser.
    val_bytes = dac_bytes(val)
    tosa_laser_bias_send(val_bytes)

    val = dacx5080_set_channel(chn, value)
    val_bytes = dac_bytes(val)
    tosa_laser_bias_send(val_bytes)

def tosa_shift_reg_toggle(tec, value):
    """ Change the state of one bit of the TEC shift register

    Arguments:
        tec (int): Channel number [0-7]
        value (bool): Enable/Disble TEC :: True/False
    """
    TOSAState().tec &= ~(2**tec)
    TOSAState().tec |= (2**tec * int(value))
    val_bytes = TOSAState().tec.to_bytes(1, byteorder='little') # byte order is irrelevant
    tosa_shift_reg_send(val_bytes)

def tosa_shift_reg(value):
    """ Overwrite tosa shift reg state and send byte

    Arguments:
        value (int): The new TOSA TEC state
    """
    TOSAState().tec = value
    val = value & 0xff
    val = shift_reg_format(val)
    val_bytes = val.to_bytes(1, byteorder='little') # byte order is irrelevant
    tosa_shift_reg_send(val_bytes)

def tosa_pic_dac(dac, value):
    pos, chn = dac
    pos = pos - 1 # 0-index
    chn = chn - 1 # 0-index

    val = dacx5080_config()
    val_bytes = pic_dac_bytes(val, pos)
    tosa_pic_dac_send(val_bytes)

    val = dacx5080_gain(gain=True, ref=False)
    val_bytes = pic_dac_bytes(val, pos)
    tosa_pic_dac_send(val_bytes)

    val = dacx5080_set_channel(chn, value)
    val_bytes = pic_dac_bytes(val, pos)
    tosa_pic_dac_send(val_bytes)

def tosa_amp_bias(channel, value):
    val = dacx5080_config()
    val_bytes = amp_bytes(val, pos)
    tosa_amp_send(val_bytes)

    val = dacx5080_gain(gain=True, ref=False)
    val_bytes = amp_bytes(val, pos)
    tosa_amp_send(val_bytes)

    val = dacx5080_set_channel(chn, value)
    val_bytes = amp_bytes(val, pos)
    tosa_amp_send(val_bytes)

def tosa_amp_shift(channel, value):
    TOSAState().tec = value
    val = value & 0xff
    val = shift_reg_format(val)
    val_bytes = val.to_bytes(1, byteorder='little') # byte order is irrelevant
    tosa_amp_shift_reg_send(val_bytes)
