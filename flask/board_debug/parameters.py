# -*- coding: utf-8 -*-
import logging
import types

def string2num(string, lookup={}):
    """ Convert enumeration to python float or int.

    Arguments:
        string (str): A parameter from an HTTP request. Either a numeric string or a label for an
            enumeration
        lookup (dict): A dict keyed on enumeration labels to enumeration values. If empty, string
            is assumed to be a numeric string
    Returns:
        The numeric string represents.
    """
    string = string.lower()
    if string in lookup.keys():
        return lookup[string]
    if type(string) is str:
        try:
            return int(string)
        except ValueError:
            return float(string)
    else:
        logging.error(f"Invalid argument value {string}")
        raise ValueError

def dsa(string):
    return int(string2num(request.args.get('dsa')))

def collection_validator(collection):
    return lambda x: x in collection

def int_validator(start, end):
    return lambda x: x in range(start, end)

def float_validator(start, end):
    return lambda x: x <= end and x >= start

class ParameterError(BaseException):
    pass

class Parameter:
    """

    Attributes:
        label (str): The parameter's key in the HTTP body
        datatype (type): The parameter's type
        converter (func): A function which takes a str as input and outputs an object of datatype.
            The HTTP value is input to this function.
        validator (func): Checks if the HTTP input is comprehesible by the server. takes one str
            as input and outputs a bool.
        default (datatype): The default value of the parameter. If this is None, the parameter is
            optional.
    """
    nop_func = lambda x: x

    def __init__(self, label, datatype, converter=None, validator=None, default=None):
        if type(label) is not str:
            raise ValueError
        if type(datatype) is not type(int):
            raise ValueError
        if types.FunctionType is not type(converter) and converter is not None:
            logging.info(type(self.nop_func).name == type(converter))
            raise ValueError
        if types.FunctionType is not type(validator) and validator is not None:
            raise ValueError
        if type(default) is not datatype and default is not None:
            raise ValueError
                
        self.label = label
        self.datatype = datatype
        self.converter = converter if converter is not None else Parameter.nop_func
        self.validator = validator if validator is not None else Parameter.nop_func
        self.default = default

    def loads(self, request):
        inp = request.args.get(self.label, None)
        if inp is None:
            if self.default is not None:
                return self.default
            else:
                raise ParameterError(f'Missing parameter {self.label}')
        val = None
        try:
            val = self.converter(inp) 
        except:
            raise ParameterError(f'Parameter {self.label} is formatted incorrectly.')
        if not self.validator(val):
            raise ParameterError(f'Parameter {self.label} has an invalid value.')
        return val

class IntParameter(Parameter):
#    def __init__(self, label, default=None):
#        if type(label) is not str:
#            raise ValueError
#        if type(default) is not int and type(default) is not None:
#            raise ValueError
    @staticmethod
    def decode(request, start, end, label, default):
        inp = request.args.get(self.label)
        if inp is None:
            if default is not None:
                return self.default
            else:
                raise ParameterError
        if not int_validator(start, end)(inp):
            raise ParameterError
