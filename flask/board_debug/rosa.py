# -*- coding: utf-8 -*-
from board_debug.dacx5080 import dacx5080_set_channel, dacx5080_config, dacx5080_gain
from board_debug.sn74hc595pwr import format_message as shift_reg_format
from board_debug.pic_dac import pic_dac_bytes
from board_debug.amp import amp_bytes
from board_debug.spi import send

APD_BIAS_CS = 1
SHIFT_REG_CS  = 2
PIC_DAC_CS    = 3 
AMP_SHIFT_CS  = 5
AMP_DAC_CS    = 6

SPI_CS = 1

class ROSAState():
    tec = 0
    _dict = {}
    def __init__(self):
        self.__dict__ = self._dict

def dac_bytes(val):
    return bytearray.fromhex("{0:0{1}x}".format(val,6))

def rosa_apd_bias_send(val_bytes):
    send(val_bytes, spi_cs=SPI_CS, cs_num=APD_BIAS_CS, direct=True, spimode=1)

def rosa_shift_reg_send(val_bytes):
    send(val_bytes, spi_cs=SPI_CS, cs_num=SHIFT_REG_CS, direct=True, spimode=0)

def rosa_pic_dac_send(val_bytes):
    send(val_bytes, spi_cs=SPI_CS, cs_num=PIC_DAC_CS, direct=True, spimode=1)

def rosa_amp_bias_send(val_bytes):
    send(val_bytes, spi_cs=SPI_CS, cs_num=AMP_DAC_CS, direct=True, spimode=1)

def rosa_amp_shift_reg_send(val_bytes):
    send(val_bytes, spi_cs=SPI_CS, cs_num=AMP_SHIFT_CS, direct=True, spimode=0)

def rosa_apd_bias(chn, value):
    val = dacx5080_config()
    val_bytes = dac_bytes(val)
    rosa_apd_bias_send(val_bytes)

    val = dacx5080_gain(gain=False, ref=False) #enabling gain could risk damage to apd.
    val_bytes = dac_bytes(val)
    rosa_apd_bias_send(val_bytes)

    val = dacx5080_set_channel(chn, value)
    val_bytes = dac_bytes(val)
    rosa_apd_bias_send(val_bytes)

def rosa_shift_reg_toggle(tec, value):
    """ Change the state of one bit of the TEC shift register

    Arguments:
        tec (int): Channel number [0-7]
        value (bool): Enable/Disble TEC :: True/False
    """
    ROSAState().tec &= ~(2**tec)
    ROSAState().tec |= (2**tec * int(value))
    val_bytes = ROSAState().tec.to_bytes(1, byteorder='little') # byte order is irrelevant
    rosa_shift_reg_send(val_bytes)

def rosa_shift_reg(value):
    """ Overwrite rosa shift reg state and send byte

    Arguments:
        value (int): The new ROSA TEC state
    """
    ROSAState().tec = value
    val = value & 0xff
    val = shift_reg_format(val)
    val_bytes = val.to_bytes(1, byteorder='little') # byte order is irrelevant
    rosa_shift_reg_send(val_bytes)

def rosa_pic_dac(dac, value):
    pos, chn = dac
    pos = pos - 1 # 0-index
    chn = chn - 1 # 0-index

    val = dacx5080_config()
    val_bytes = pic_dac_bytes(val, pos)
    rosa_pic_dac_send(val_bytes)

    val = dacx5080_gain(gain=True, ref=False)
    val_bytes = pic_dac_bytes(val, pos)
    rosa_pic_dac_send(val_bytes)

    val = dacx5080_set_channel(chn, value)
    val_bytes = pic_dac_bytes(val, pos)
    rosa_pic_dac_send(val_bytes)

def rosa_amp_bias(channel, value):
    val = dacx5080_config()
    val_bytes = amp_bytes(val, pos)
    rosa_amp_send(val_bytes)

    val = dacx5080_gain(gain=True, ref=False)
    val_bytes = amp_bytes(val, pos)
    rosa_amp_send(val_bytes)

    val = dacx5080_set_channel(chn, value)
    val_bytes = amp_bytes(val, pos)
    rosa_amp_send(val_bytes)

def rosa_amp_shift(channel, value):
    ROSAState().tec = value
    val = value & 0xff
    val = shift_reg_format(val)
    val_bytes = val.to_bytes(1, byteorder='little') # byte order is irrelevant
    rosa_amp_shift_reg_send(val_bytes)
