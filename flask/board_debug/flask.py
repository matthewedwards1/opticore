# -*- coding: utf-8 -*-
from enum import Enum
import logging

from flask import request
from flask import render_template

from board_debug.api import load_api
import board_debug.spi


def start_server(args, app):
    load_api(app)
    load_pages(app)

    try:
        app.run(host=args.ip, port=args.port, debug=args.debug is not None)
    except OSError as e:
        logging.error("Unable to start flask server. Ensure the IP and port specified are available.")
        logging.error(f"IP: {args.ip}, Port: {args.port}")

def load_pages(app):
    @app.route('/', methods=["GET", "POST"])
    def homepage():
        return render_template('index.html')

