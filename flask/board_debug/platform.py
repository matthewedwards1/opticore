# -*- coding: utf-8 -*-

from enum import Enum
import os

class Platform(Enum):
    UNKNOWN = 0
    PC = 1
    PI = 2

def get_platform():
    try:
        import spidev
        return Platform.PI
    except Exception as e: # Non-unix platform
        return Platform.PC

