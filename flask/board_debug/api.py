# -*- coding: utf-8 -*-
import json
import pprint
import logging
import struct
import math
import os
import time
import pdb

from flask import request
from flask import jsonify
from flask import Response

from board_debug.devices import *
from board_debug.spi import send
from board_debug.parameters import string2num, Parameter, ParameterError
from board_debug.i2c import pic_dac_read_i2c
from board_debug.ltc2991 import read_voltage, set_zero, set_gain
from board_debug.pic_dac import pic_dac_execute
from board_debug.tosa import tosa_laser_bias, tosa_shift_reg, tosa_pic_dac, tosa_shift_reg_toggle
from board_debug.tosa import tosa_amp_bias, tosa_amp_shift
from board_debug.rosa import rosa_apd_bias
from board_debug.tmp100na3k import get_temp

BOARD_OFFSET = 29
DEVICE_OFFSET = 24

def generate_report(request, data={}):
    pp = pprint.PrettyPrinter(indent=4)

    args_dict = {**request.args}
    args_str = pp.pformat(args_dict)

    request_dict = {key: value for key, value in vars(request).items() if key != "environ"}
    request_str = pp.pformat(request_dict)

    return "".join(["<h3>You sent the following parameters:</h3>",
                   f"<pre>{args_str}</pre>",
                   f"<h3>For request</h3>",
                   f"<pre>{request_str}</pre>"])

def parameter_error_report(e):
    return Response(f"<h2 style=\"color: red\">{e}</h2><br>{generate_report(request)}", 422)


def reverse_endianness_24(val):
    return [(val & (0xff << pos*8)) >> pos*8 for pos in reversed(range(3))]

def reverse_endianness_32(val):
    return [(val & (0xff << pos*8)) >> pos*8 for pos in reversed(range(4))]

def bytes_32(val):
    return [(val & (0xff << pos*8)) >> pos*8 for pos in range(4)]

def bytes_24(val):
    return [(val & (0xff << pos*8)) >> pos*8 for pos in range(3)]

def update_switch_state(old_state, switch, value):
    old_state &= ~(0x1 << switch)          # Clear old state
    old_state |= (int(value!=0) << switch) # Write new state
    return old_state

# XXX
class SwitchState():
    # Borg pattern
    _state = {
    }
    def __init__(self):
        self.__dict__ = self._state


def load_api(app):

    @app.route('/pic_dac', methods=["GET", "POST"])
    def pic_dac():
        # Get parameters
        try:
            dacparam = Parameter('dac', int,
                converter=lambda x: string2num(x, PIC),
                validator=lambda x: True)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            dac = dacparam.loads(request)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)

        pic_dac_execute(dac, value)

        return generate_report(request)

    @app.route('/pic_dac_reset', methods=["GET", "POST"])
    def pic_dac_reset():
        for dac in range(1, 9):
            for channel in range(1, 9):
                pic_dac_execute((dac, channel), 0)
        return generate_report(request)

    @app.route('/pic_dac_all', methods=["GET", "POST"])
    def pic_dac_all():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        for dac in range(1, 9):
            for channel in range(1, 9):
                pic_dac_execute((dac, channel), value)
        return generate_report(request)

    @app.route('/pic_dac_read_i2c_raw', methods=["GET", "POST"])
    def pic_dac_read_i2c_raw():
        try:
            i2cparam = Parameter('i2c', int,
                converter=lambda x: string2num(x, PIC_DAC_I2C),
                validator=lambda x: True)
            i2c = i2cparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        return jsonify(str(pic_dac_read_i2c(*i2c)))

    @app.route('/pic_dac_read_adc', methods=["GET", "POST"])
    def pic_dac_read_adc():
        def conv(x):
            if x == "inside":
                return 1
            if x == "outside":
                return 0
            return int(x)
        try:
            muxparam = Parameter('mux', int, converter=conv,
                validator=lambda x: x == 0 or x == 1)
            mux = muxparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        return jsonify(str(read_voltage(mux)))

    @app.route('/pic_dac_set_zero', methods=["GET", "POST"])
    def pic_dac_set_zero():
        def conv(x):
            if x == "inside":
                return 1
            if x == "outside":
                return 0
            return int(x)
        try:
            muxparam = Parameter('mux', int, converter=conv,
                validator=lambda x: x == 0 or x == 1)
            mux = muxparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        set_zero(mux)
        return generate_report(request)

    @app.route('/pic_dac_set_isense_gain', methods=["GET", "POST"])
    def pic_dac_set_isense_gain():
        try:
            onparam = Parameter('on', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            on = onparam.loads(request)
            deviceparam = Parameter('device', int, converter=lambda x: x,
                validator=lambda x: x == 'inside' or x == 'outside')
            device = deviceparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        set_gain(device, on)
        return generate_report(request)

    @app.route('/tosa_laser_bias', methods=["GET", "POST"])
    def tosa_las_bias():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
            channelparam = Parameter('channel', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: x < 8 and x >= 0)
            channel = channelparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        tosa_laser_bias(channel, value)
        return generate_report(request)

    @app.route('/tosa_shift_reg', methods=["GET", "POST"])
    def tosa_shft_reg():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: True)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        tosa_shift_reg(value)
        return generate_report(request)

    @app.route('/tosa_tec', methods=["GET", "POST"])
    def tosa_tec():
        try:
            tecparam = Parameter('tec', int,
                converter=lambda x: string2num(x, TOSA_TEC),
                validator=lambda x: True)
            tec = tecparam.loads(request)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        tosa_shift_reg_toggle(tec, value)
        return generate_report(request)

    @app.route('/tosa_shift_reg_on', methods=["GET", "POST"])
    def tosa_shft_reg_on():
        tosa_shift_reg(0xff)
        return generate_report(request)

    @app.route('/tosa_shift_reg_off', methods=["GET", "POST"])
    def tosa_shft_reg_off():
        tosa_shift_reg(0x00)
        return generate_report(request)

    @app.route('/tosa_pic_dac', methods=["GET", "POST"])
    def tosa_pdac():
        try:
            dacparam = Parameter('dac', int,
                converter=lambda x: string2num(x, PIC),
                validator=lambda x: True)
            dac = dacparam.loads(request)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        tosa_pic_dac(dac, value)
        return generate_report(request)

    @app.route('/tosa_amp_bias', methods=["GET", "POST"])
    def tosa_ampbias():
        try:
            dacparam = Parameter('dac', int,
                converter=lambda x: string2num(x, PIC),
                validator=lambda x: True)
            dac = dacparam.loads(request)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        tosa_amp_bias(dac, value)
        return generate_report(request)

    @app.route('/tosa_amp_dsa', methods=["GET", "POST"])
    def tosa_ampshift():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        tosa_amp_shift(value)
        return generate_report(request)

    @app.route('/tosa_read_temp', methods=["GET", "POST"])
    def tosa_read_temp():
        try:
            tempparam = Parameter('temp', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: x == 0 or x == 1 or x == 2 or x == 3)
            temp = tempparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        return jsonify(str(get_temp(temp)))

    @app.route('/rosa_apd_bias', methods=["GET", "POST"])
    def rosa_bias():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
            channelparam = Parameter('channel', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: x < 8 and x >= 0)
            channel = channelparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        rosa_apd_bias(channel, value)
        return generate_report(request)

    @app.route('/rosa_shift_reg', methods=["GET", "POST"])
    def rosa_shft_reg():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: True)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        rosa_shift_reg(value)
        return generate_report(request)

    @app.route('/rosa_tec', methods=["GET", "POST"])
    def rosa_tec():
        try:
            tecparam = Parameter('tec', int,
                converter=lambda x: string2num(x, TOSA_TEC),
                validator=lambda x: True)
            tec = tecparam.loads(request)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        rosa_shift_reg_toggle(tec, value)
        return generate_report(request)

    @app.route('/rosa_shift_reg_on', methods=["GET", "POST"])
    def rosa_shft_reg_on():
        rosa_shift_reg(0xff)
        return generate_report(request)

    @app.route('/rosa_shift_reg_off', methods=["GET", "POST"])
    def rosa_shft_reg_off():
        rosa_shift_reg(0x00)
        return generate_report(request)

    @app.route('/rosa_pic_dac', methods=["GET", "POST"])
    def rosa_pdac():
        try:
            dacparam = Parameter('dac', int,
                converter=lambda x: string2num(x, PIC),
                validator=lambda x: True)
            dac = dacparam.loads(request)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        rosa_pic_dac(dac, value)
        return generate_report(request)

    @app.route('/rosa_amp_bias', methods=["GET", "POST"])
    def rosa_ampbias():
        try:
            dacparam = Parameter('dac', int,
                converter=lambda x: string2num(x, PIC),
                validator=lambda x: True)
            dac = dacparam.loads(request)
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        rosa_amp_bias(dac, value)
        return generate_report(request)

    @app.route('/rosa_amp_dsa', methods=["GET", "POST"])
    def rosa_ampshift():
        try:
            valueparam = Parameter('value', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: bool(x) is True or bool(x) is False)
            value = valueparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        rosa_amp_shift(value)
        return generate_report(request)

    @app.route('/rosa_read_temp', methods=["GET", "POST"])
    def rosa_read_temp():
        try:
            tempparam = Parameter('temp', int, converter=lambda x: int(string2num(x)),
                validator=lambda x: x == 0 or x == 1 or x == 2 or x == 3)
            temp = tempparam.loads(request)
        except ParameterError as e:
            return parameter_error_report(e)
        return jsonify(str(get_temp(temp)))
