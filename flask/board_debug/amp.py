# -*- coding: utf-8 -*-

def amp_bytes(val):
    return bytearray.fromhex("{0:0{1}x}".format(val,6))
