# -*- coding: utf-8 -*-
import argparse
import logging
import os

from flask import Flask

from board_debug.args import args
from board_debug.flask import start_server
import board_debug.settings as settings

def initialize_logs():
    # Make log dir, set log dir
    os.makedirs('log', exist_ok=True)

    log = logging.getLogger('werkzeug')
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S',
                        filename=os.path.join('log', 'debug.log'),
                        filemode='a')
    print("Done setting up log")

def initialize_flask(root):
    return Flask(root)

def start(root=__name__):
    initialize_logs()
    app = initialize_flask(root)
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
    start_server(args, app)
