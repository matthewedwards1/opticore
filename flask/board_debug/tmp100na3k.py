# -*- coding: utf-8 -*-

from .i2c import read_word_i2c as read
from .i2c import pic_dac_write_i2c as write

TMP1_ADDR = 0x4C
TMP2_ADDR = 0x4D
TMP3_ADDR = 0x4A
TMP4_ADDR = 0x4B

TMPS = [
    TMP1_ADDR,
    TMP2_ADDR,
    TMP3_ADDR,
    TMP4_ADDR,
]

TMP_REG    = 0x0
CONFIG_REG = 0x1

def hex2temp(h):
    """ 12-bit 2's complement
    """
    sign = -1 if h & 0x800 else 1
    mantissa = (h & 0x7FF) if sign == 1 else (~h & 0x7FF) + 1
    return (sign * mantissa)/16

def config_payload():
    return 0xb0

def send_config(tmp):
    tmp_addr = TMPS[tmp]
    write(tmp_addr, CONFIG_REG, config_payload())

def read_temp(tmp):
    tmp_addr = TMPS[tmp]
    b = read(tmp_addr, TMP_REG)
    return (((b & 0xff) << 8) | ((b & 0xff00) >> 8)) >> 4

def get_temp(tmp):
    send_config(tmp)
    b = read_temp(tmp)
    return hex2temp(b)
