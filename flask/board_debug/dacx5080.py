# -*- coding: utf-8 -*-

READ  = 1
WRITE = 0

# Bit formatting
DACX5080_READ_OFFSET  = 23
DACX5080_ADDR_OFFSET  = 16
DACX5080_VALUE_OFFSET = 4

# Register bits
DACX5080_CONFIG         = 3
DACX5080_GAIN           = 4
DACX5080_CHANNEL_OFFSET = 8

# Config bits
DACX5080_INTERNAL_REF   = 1 << 8

# Gain bits
DACX5080_REF_DIV_EN_POS = 8
DACX5080_REF_DIV_EN     = 1 << DACX5080_REF_DIV_EN_POS

def dacx5080_channel_command(read, address, data):
    """ Generate a binary command for DACX5080 TI devices.
    
    Arguments:
        read (bool): True if a read is requested, False if a write is requested.
        address (int): 0 - 7, the target channel.
        data (int): The channel value.

    Returns:
        An integer represented the DAC command.
    """
    return (int(read) << DACX5080_READ_OFFSET) \
           | (address << DACX5080_ADDR_OFFSET) \
           | (data << DACX5080_VALUE_OFFSET)

def dacx5080_command(read, address, data):
    return (int(read) << DACX5080_READ_OFFSET) \
           | (address << DACX5080_ADDR_OFFSET) \
           | (data)

def dacx5080_set_channel(channel, value):
    return dacx5080_channel_command(WRITE, channel + DACX5080_CHANNEL_OFFSET, value)

def dacx5080_config():
    return dacx5080_command(WRITE, DACX5080_CONFIG, DACX5080_INTERNAL_REF)

def dacx5080_gain(gain=False, ref=False):
    gain_payload = (0xff * int(gain)) \
                   | ((0x1 * int(ref)) << DACX5080_REF_DIV_EN_POS)
    return dacx5080_command(WRITE, DACX5080_GAIN, gain_payload)
