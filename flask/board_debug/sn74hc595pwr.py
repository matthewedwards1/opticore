# -*- coding: utf-8 -*-

def format_message(num):
    """ Format byte so that the most significant byte is sent first 
    when raspi spidev mode is 0 or 2


    Arguments:
        num (int): The byte to format
    Returns:
        The input byte with flipped bitorder
    
    """
    return num
