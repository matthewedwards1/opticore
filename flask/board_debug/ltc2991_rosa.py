from .i2c import rosa_read_i2c as read
from .i2c import rosa_write_i2c as write

WRITE_ADDR   = 0x77 # I2C bus addr for LTC 2991 write 
WRITE_REG    = 0x1  # Trigger read reg
WRITE_VAL    = 0xff # Trigger read data

READ_ADDR    = 0x4B
READ_REG_MSB_BASE = 0xA
READ_REG_LSB_BASE = 0xB

ADC_RESOLUTION = 305.18 * 1e-6

MAX_RETRIES = 5

class Cal():
    outside = 0
    inside  = 0
    _dict = {}
    def __init__(self):
        self.__dict__ = self._dict

    def get_voltage(self, channel):
        if channel:
           return self.outside
        else:
           return self.inside
    
    def set_voltage(self, channel, val):
        if channel:
            self.outside = val 
        else:
            self.inside = val 

def adc_data_is_new(msb):
    data_is_new = ((msb & 0x40) != 0)
    return data_is_new

def bytes_to_voltage(data):
    return ADC_RESOLUTION * data

def decode_adc_bytes(channel, msb, lsb):
    payload = (((msb & 0x3F) << 8) | lsb) * ((-1) ** ((msb & 0x40) >> 7))
    return bytes_to_voltage(payload) - Cal().get_voltage(channel)

def read_ltc2991(channel):
    write(WRITE_ADDR, WRITE_REG, WRITE_VAL)
    msb_addr = READ_REG_MSB_BASE + channel*2
    lsb_addr = READ_REG_LSB_BASE + channel*2
    msb = read(READ_ADDR, msb_addr)
    lsb = read(READ_ADDR, lsb_addr)
    return msb, lsb

def read_voltage_rosa(channel):
    msb, lsb = read_ltc2991(channel)
    retries = 0
    while (not adc_data_is_new(msb)) and retries < MAX_RETRIES:
        msb, lsb = read_ltc2991(channel)
        retries += 1
    return decode_adc_bytes(channel, msb, lsb)

def set_zero(channel=False):
    Cal().set_voltage(channel, read_voltage_rosa(channel))
