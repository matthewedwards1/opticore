# -*- coding: utf-8 -*-
import argparse

from board_debug import settings

HELP = {
'ip':     "IP Address of the server",
'port':   "Port of the server",
'debug':  "Enable debug mode",
'config': "The location of the server config file. Overrides default config.",
}

ARGS = {
'--ip':     {'action':'store', 'type':str, 'default':settings.host.ip, 'help':HELP['ip']},
'--port':   {'action':'store', 'type':int, 'default':settings.host.port, 'help':HELP['port']},
'--debug':  {'action':'count', 'help':HELP['debug']},
'--config': {'action':'store', 'help':HELP['config']},
}

parser = argparse.ArgumentParser(description='board_debug')
for arg, kwargs in ARGS.items():
    parser.add_argument(arg, **kwargs)
args = parser.parse_args()
