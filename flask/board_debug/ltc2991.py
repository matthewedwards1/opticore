from .i2c import pic_dac_read_i2c as read
from .i2c import pic_dac_write_i2c as write

MUX_WRITE_ADDR   = 0x77 # I2C bus addr for LTC 2991 write 
MUX_WRITE_REG    = 0x1  # Trigger read reg
MUX_WRITE_VAL    = 0xff # Trigger read data

MUX1_READ_ADDR    = 0x48
MUX2_READ_ADDR    = 0x49
MUX_READ_REG_MSB = 0xA
MUX_READ_REG_LSB = 0xB

GAIN_WRITE_ADDR = 0x20
GAIN_WRITE_REG  = 0x00

ADC_RESOLUTION = 305.18 * 1e-6

MAX_RETRIES = 5

BUS_NUM = 1 # I2C Bus

class Cal():
    outside = 0
    inside  = 0
    _dict = {}
    def __init__(self):
        self.__dict__ = self._dict

    def get_voltage(self, mux):
        if mux:
           return self.outside
        else:
           return self.inside
    
    def set_voltage(self, mux, val):
        if mux:
            self.outside = val 
        else:
            self.inside = val 

def adc_data_is_new(msb):
    data_is_new = ((msb & 0x40) != 0)
    return data_is_new

def bytes_to_voltage(data):
    return ADC_RESOLUTION * data

def decode_adc_bytes(mux, msb, lsb):
    payload = (((msb & 0x3F) << 8) | lsb) * ((-1) ** ((msb & 0x40) >> 7))
    return bytes_to_voltage(payload) - Cal().get_voltage(mux)

def read_ltc2991(mux=False):
    read_addr = MUX1_READ_ADDR
    if mux:
        read_addr = MUX2_READ_ADDR
    write(MUX_WRITE_ADDR, MUX_WRITE_REG, MUX_WRITE_VAL)
    msb = read(read_addr, MUX_READ_REG_MSB)
    lsb = read(read_addr, MUX_READ_REG_LSB)
    return msb, lsb

def read_voltage(mux=False): # TODO: Add arg to choose which current sensor to read from (0x49 or 0x48)
    msb, lsb = read_ltc2991(mux)
    retries = 0
    while (not adc_data_is_new(msb)) and retries < MAX_RETRIES:
        msb, lsb = read_ltc2991()
        retries += 1
    return decode_adc_bytes(mux, msb, lsb)

def set_zero(mux=False):
    Cal().set_voltage(mux, read_voltage(mux))

def set_gain(choice, on):
    reg = 0
    if choice == 'inside':
        reg = 0
    elif choice == 'outside':
        reg = 1
    else:
        return

    current_setting = read(GAIN_WRITE_ADDR)
    current_setting &= ~(0x1 << reg)
    current_setting |= ((0x1 if on else 0x0) << reg) 
    write(GAIN_WRITE_ADDR, GAIN_WRITE_REG, current_setting)
