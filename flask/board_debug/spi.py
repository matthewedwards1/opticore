# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
import logging
import time

from board_debug import platform
from board_debug.platform import Platform

DIRECT_CS0 = (11, 13, 15)
DIRECT_CS1 = (29, 31, 33)
DIRECT_CS = [DIRECT_CS0, DIRECT_CS1]

class SerialControl(ABC):
    """ A class which manages the serial connection between a Device object and a serial
    peripheral.

    Arguments:
        name (str): A path to a serial device on the machine running the GXC Command server.
        conn (Serial): A Serial object which sends serial commands to the serial device specified.
        id (int): The id of this serial control object.
    """

    @abstractmethod
    def get_serial_options():
        """ Lists serial configuration options. If the SerialController child, for instance,
        operates over USB, the output of this function may be the USB devices the operating system
        can detect. If the SerialController child is, for instance, a SPI device, then the output
        of this function may be a list of SPI configurations

        Raises:
            EnvironmentError: On unsupported or unknown platforms.
        Returns:
            A 2-tuple.

            The first item in the tuple is a list of strings describing the serial
            options available. In the case of USB, OS-level USB interfaces are returned. In the
            case of SPI, SPI Clock frequencies are returned.

            The second item in the tuple is a string indicating the type of option available.
        """
        pass

    @abstractmethod
    def is_connected(self):
        """ Check if serial connection is open.
        """
        pass

if platform == Platform.PI:

    import RPi.GPIO as GPIO
    import spidev

    class SerialControlRaspPiSPI(SerialControl):
        valid_clk_freqs = {125000000, # Units = Hz
                           62500000,
                           31200000,
                           15600000,
                           7800000,
                           3900000,
                           1953000,
                           976000,
                           488000,
                           244000,
                           122000,
                           61000,
                           30500,
                           15200,
                           7629}

        default_clk_freq = 976000

        def __init__(self, clk_freq=976000, spi_device=0, spi_cs=0, chip_select=0, spimode=1):
            self.clk_freq = clk_freq if clk_freq in self.valid_clk_freqs else self.default_clk_freq
            self.rpi_spi = spidev.SpiDev()
            self.rpi_spi.open(spi_device, chip_select)
            self.rpi_spi.max_speed_hz = self.clk_freq
            self.rpi_spi.mode = spimode

        def __del__(self):
            self.rpi_spi.close()

        @classmethod
        def get_serial_options(cls):
            return list(SerialControlRaspPiSPI.valid_clk_freqs), "SPI clock frequencies"

        def is_connected(self):
            pass

        def send(self, val_hex):
            self.rpi_spi.xfer(val_hex)

        def read(self):
            pass

    class SerialControlDirectRaspPiSPI(SerialControlRaspPiSPI):
        def __init__(self, clk_freq=976000, spi_device=0, spi_cs=0, chip_select=0, spimode=1):
            self.clk_freq = clk_freq if clk_freq in self.valid_clk_freqs else self.default_clk_freq
            self.rpi_spi = spidev.SpiDev()
            self.rpi_spi.open(spi_device, spi_cs)
            self.rpi_spi.max_speed_hz = self.clk_freq
            self.rpi_spi.mode = spimode

            self.csmap = DIRECT_CS[chip_select]

            # Set up chip selects
            self.chip_select = chip_select
            GPIO.setmode(GPIO.BOARD)
            for cs in range(3):
                if (chip_select >> cs) & 0x1:
                    GPIO.setup(self.csmap[cs], GPIO.OUT)
                    GPIO.output(self.csmap[cs], GPIO.HIGH)
                else:
                    GPIO.setup(self.csmap[cs], GPIO.OUT)
                    GPIO.output(self.csmap[cs], GPIO.LOW)

        def csup(self):
            GPIO.setmode(GPIO.BOARD)
            for cs in range(3):
                if (self.chip_select >> cs) & 0x1:
                    GPIO.setup(self.csmap[cs], GPIO.OUT)
                    GPIO.output(self.csmap[cs], GPIO.HIGH)
                else:
                    GPIO.setup(self.csmap[cs], GPIO.OUT)
                    GPIO.output(self.csmap[cs], GPIO.LOW)

        def csdown(self):
            for cs in range(3):
                GPIO.setup(self.csmap[cs], GPIO.OUT)
                GPIO.output(self.csmap[cs], GPIO.LOW)

        def __del__(self):
            self.rpi_spi.close()

            for cs in range(3):
                GPIO.setup(self.csmap[cs], GPIO.OUT)
                GPIO.output(self.csmap[cs], GPIO.LOW)

        def send(self, val_hex):
            self.csup()
            self.rpi_spi.xfer(val_hex)
            self.csdown()

    def sendPI(pkt, spi_dev=0, spi_cs=0, cs_num=0, direct=False, spimode=1):
        if direct:
            control = SerialControlDirectRaspPiSPI(spi_device=spi_dev, spi_cs=spi_cs, chip_select=cs_num, spimode=spimode)
            status = control.send(pkt)
        else:
            control = SerialControlRaspPiSPI(chip_select=cs_num, spimode=spimode)
            status = control.send(pkt)
        logging.info(f"Sent {len(pkt)} bytes")
        time.sleep(0.05)

def sendPC(*args, **kwargs):
    try:
        logging.info(f"Sent {len(pkt)} bytes on CS {cs_num}")
    except:
        logging.info(f"SendPC({args} {kwargs})")
        pass

def send(*args, **kwargs):
    if platform == Platform.PC:
        sendPC(*args, **kwargs)
    elif platform == Platform.PI:
        sendPI(*args, **kwargs)
