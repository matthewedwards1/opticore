# -*- coding: utf-8 -*-
from board_debug.dacx5080 import dacx5080_set_channel, dacx5080_config, dacx5080_gain
from board_debug.spi import send

N_DACS = 8

def pic_dac_bytes(val, spi_bus_pos):
    # the dacs have their spi bus daisy-chained.  'padded' gets
    # no-op commands (zeroed bytes) for all but the dac we want to write to.
    payload = bytearray.fromhex("{0:0{1}x}".format(val,6))
    padded = bytearray()
    for i in reversed(range(N_DACS)):
        if spi_bus_pos == i:
            padded = padded + payload
        else:
            padded = padded + bytearray([0x00, 0x00, 0x00]) # command length 24
    return padded

def pic_dac_execute(dac, value):
    pos, chn = dac
    pos = pos - 1 # 0-index
    chn = chn - 1 # 0-index

    val = dacx5080_config()
    val_bytes = pic_dac_bytes(val, pos)
    send(val_bytes, spimode=1)

    val = dacx5080_gain(gain=True, ref=False)
    val_bytes = pic_dac_bytes(val, pos)
    send(val_bytes, spimode=1)

    val = dacx5080_set_channel(chn, value)
    val_bytes = pic_dac_bytes(val, pos)
    send(val_bytes, spimode=1)
