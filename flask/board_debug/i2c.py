from board_debug import platform
from board_debug.platform import Platform

import logging

BUS_NUM = 1 # I2C Bus

def get_bus():
   if platform == Platform.PC:
       return None
   if platform == Platform.PI:
        import smbus 
        return smbus.SMBus(BUS_NUM)

def pic_dac_read_adc_driver():
    try:
        return _acquire_adc_bytes(get_bus())
    except AttributeError:
        logging.warn('I2C not supported on this device')

def pic_dac_write_i2c(io_addr, data_addr, value):
    try:
        get_bus().write_byte_data(io_addr, data_addr, value)
    except AttributeError:
        logging.warn('I2C not supported on this device')
        

def pic_dac_read_i2c(io_addr, data_addr=None):
    try:
        if data_addr is not None:
            return get_bus().read_byte_data(io_addr, data_addr)
        else:
            return get_bus().read_byte(io_addr)
    except AttributeError:
        logging.warn('I2C not supported on this device')

def read_word_i2c(io_addr, data_addr=None):
    try:
        if data_addr is not None:
            return get_bus().read_word_data(io_addr, data_addr)
        else:
            return get_bus().read_word(io_addr)
    except AttributeError:
        logging.warn('I2C not supported on this device')
