# -*- coding: utf-8 -*-
import argparse
from argparse import Namespace
import os

import toml

DEFAULT_TOML = os.path.join(os.path.dirname(__file__), os.pardir, 'config', 'config.toml')

def load_settings(tomlfile):
    settings_dict = toml.loads(open(tomlfile, 'r').read())
    def dict_to_namespace(d):
        for k, v in d.items():
            if type(v) is dict:
                d[k] = dict_to_namespace(v)
        return Namespace(**d)
    for k, v in settings_dict.items():
        if type(v) is dict:
            settings_dict[k] = dict_to_namespace(v)
    globals().update(settings_dict)
    globals().update({'loaded': True})

if 'loaded' not in globals().keys():
    settings_parser = argparse.ArgumentParser(description='settings', add_help=False)
    settings_parser.add_argument('--config', action='store', type=str, default=DEFAULT_TOML)
    settings_args, unknown = settings_parser.parse_known_args()
    load_settings(settings_args.config)
