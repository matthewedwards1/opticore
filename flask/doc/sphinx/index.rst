.. OptiCore documentation master file, created by
   sphinx-quickstart on Thu Jan  7 09:28:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OptiCore's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

|br|
.. http:get:: /pic_dac

   Send a byte payload directly from the Pi SPI hardware to a PIC DAC board.

   **Example request**:

   .. sourcecode:: http

      GET /pic_dac?dac=i15&value=2047 HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]

   :param dac: The target DAC channel on the PIC DAC board. e.g. i15, o25
   :param value: The value loaded into the DAC channel. 0 - 4095
|br|
.. http:get:: /pic_dac_reset

   Set to 0 all DAC channels of a PIC DAC directly connected to a Pi's SPI Hardware.

   **Example request**:

   .. sourcecode:: http

      GET /pic_dac_reset
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]
|br|
.. http:get:: /pic_dac_all

   Set all DAC channels of a PIC DAC directly connected to a Pi's SPI Hardware.

   **Example request**:

   .. sourcecode:: http

      GET /pic_dac_all?value=103
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]

   :param value: The value to set all DAC channels to. 0 - 4095.
|br|
.. http:get:: /pic_dac_read_adc

   Read ADC voltage from PIC DAC. Return the current detected

   **Example request**:

   .. sourcecode:: http

      GET /pic_dac_read_adc
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
         3.02918490
      ]
|br|
.. http:get:: /pic_dac_set_isense_gain

   Set 1x or 10x gain for the current sense gain.

   **Example request**:

   .. sourcecode:: http

      GET /pic_dac_set_isense_gain?on=1&device=inside HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [

   :param on: Set 10x gain on or off
   :param device: "inside" or "outside". The target isense gain amp

|br|
.. http:get:: /tosa_laser_bias

   Send a DAC channel on the TOSA laser bias control DAC

   **Example request**:

   .. sourcecode:: http

      GET /tosa_laser_bias?value=1048&channel=4 HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]

   :param value: The value to set the DAC channel to. 0 - 4095
   :param channel: The target DAC channel. 0 - 7
|br|
.. http:get:: /tosa_shift_reg

   Send a DAC channel on the TOSA laser bias control DAC

   **Example request**:

   .. sourcecode:: http

      GET /tosa_shift_reg?value=1048 HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]

   :param value: The value to set the shift register to. 0 - 255
|br|
.. http:get:: /tosa_tec

   Toggle a single bit in the TOSA TEC shift register

   **Example request**:

   .. sourcecode:: http

      GET /tosa_shift_reg?tec=4&value=1 HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]

   :param value: 1 to set the shift register bit, 0 to clear it.
   :param tec: A target TEC. 0 - 7.
|br|
.. http:get:: /tosa_shift_reg_off

   Clear all channels in the TOSA shift reg

   **Example request**:

   .. sourcecode:: http

      GET /tosa_shift_reg_off HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]
|br|
.. http:get:: /tosa_shift_reg_on

   Set all channels in the TOSA shift reg

   **Example request**:

   .. sourcecode:: http

      GET /tosa_shift_reg_on HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]
|br|
.. http:get:: /tosa_pic_dac

   Set a channel on a PIC DAC connected to a TOSA connector 0

   **Example request**:

   .. sourcecode:: http

      GET /tosa_pic_dac?value=0&dac=i15 HTTP/1.1
      host: localhost:8000
      Accept: application/json, text/javascript

   **Example response**: 

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: text/html
      [
      ]

   :param dac: The target DAC channel on the PIC DAC board. e.g. i15, o25
   :param value: The value loaded into the DAC channel. 0 - 4095
|br|


.. |br| raw:: html

   <br>
