# Startup Server

## Python setup

Ensure that `python` is python3.

Check the output of `python -V`

If `python` is python 2, then you can add the following to `~/.bashrc`:

`alias python=python3`

## raspi-config

### Drivers
Enter `sudo raspi-config`

Go to option 5, interface options

Enable I2C and SPI. Optionally enable SSH and VNC

### Wifi

Go to option 2, select WiFi, enter SSID and PW of your WiFi network

## Install 
`python -m pip install -r requirements.txt`

## Run
`python main.py [--debug]`

# Send commands to the server

## In browser URL bar

http://<IP>:8000/pic_dac?dac=i26&value=g43wgv

Replace <IP> with the IP address of the Pi

e.g. http://192.168.0.139:8000/pic_dac?dac=i26&value=0

## From python

```
import requests
requests.get(http://192.168.0.139:8000/pic_dac, params={"dac":"i26, "value":0})
```

