from setuptools import setup, find_packages

setup(
    name='electrooptical_drivers',
    version='0.1dev',
    license='MIT',
    packages=find_packages(),
    data_files = [('config', ['config/config.toml']),
                 ]
)

